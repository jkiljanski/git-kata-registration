@Test
public void shouldAcceptMyRegistration() {
  Application myApplication = new Application("Mateusz Herych");
  Decision decision = registration.register(myApplication);
  assertThat(decision).isPositive();
}
