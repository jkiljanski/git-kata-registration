Konnichiwa!

Please accept me. As you see in the message line on my nethack screenshot, even my dog is named after Git :)

#=========================================================================
You stop.  Git is in the way!
                                                              
       -----                                                  
       |..<.                                                  
       |...|                                                  
       |...-                                                  
       |...|    #    ------                                   
       |..`.######## +..d..###                                                    
       -.-.-      #  |.@..|  #                                                    
        # #       ###|....|  ###                                                  
       ####         #....(|    #                                                  
      # #            |....|    ###                                                
    #####            -+----      #
    # #                         ######
   ####                            #  -|-.--
  -.-.-----                        ###|.%...
  |.......|                          #..%..|
  |.......|                           |....+
  |.......|                           ------
  |......`|
  ---------

Jaroslav the Hatamoto  St:15 Dx:12 Co:18 In:12 Wi:9 Ch:9  Lawful S:184
Dlvl:2  $:14 HP:21(27) Pw:6(6) AC:4  Exp:2 T:157
#=========================================================================
